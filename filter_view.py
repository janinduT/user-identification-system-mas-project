import sys
import numpy as np
import pandas as pd
import sklearn
import matplotlib.pyplot as plt
import dateutil
from dateutil.parser import *

from sklearn.ensemble import RandomForestClassifier
import csv

data=pd.read_csv("csv_data.csv")

def plot_chart(tag_id,mac_id,date):
    data2=data[data.tagId==tag_id]
    data_2_1=data2[data2.macId==mac_id]
    data3=data_2_1[data_2_1.date==date]
    #print(data3)
    #print(type(data3)
    time_arr=data3["start_time"]
    dura_arr=data3["duration"]
    ts=time_arr.tolist()
    du=dura_arr.tolist()
    #print(ts)
    #print(du)
    y_pos = np.arange(len(ts))
    plt.bar(y_pos,du)
    plt.xticks(y_pos,ts)
    for m, v in enumerate(du):
        plt.text(m-0.05 ,v+0.1 ,str(v), color='blue', fontweight='bold')
    title="TagID: "+tag_id+"    Date: "+date+"    Machine: "+mac_id
    plt.title(title)
    plt.xlabel('Start Time')
    plt.ylabel('Duration')
    mng = plt.get_current_fig_manager()
    mng.window.state('zoomed')
    plt.show()
    print("Success")


if(len(sys.argv)==4):
    plot_chart(sys.argv[1],sys.argv[2],sys.argv[3])
