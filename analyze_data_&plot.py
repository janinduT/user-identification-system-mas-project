#pythonforengineers.com/machine-learning-for-complete-beginners
#kaggle.com/kanncaa1/data-sciencetutorial-for-beginners

#sessions array format [[tagID,[macID,[date,diff(1,2),diff(2,3),...],[date,diff..]]],[tagID[macID[][]]]]

import numpy as np
import pandas as pd
import sklearn
import matplotlib.pyplot as plt
import dateutil
from dateutil.parser import *

from sklearn.ensemble import RandomForestClassifier
import csv

data=pd.read_csv("F:/data.csv")
print(type(data))
#data=data[["recID","rTime"]]
tag=data.tagID
day=data.rDate
machine=data.macID
#print(tag)
tags=[]
sessions=[]
days=[]
machines=[]
for i in tag:
    if i not in tags:
        tags.append(i)
print(tags)

for i in day:
    if i not in days:
        days.append(i)
print(days)

for i in machine:
    if i not in machines:
        machines.append(i)
print(machines)

for i in tags:
    tagd=[]
    tagd.append(i)
    data_1_1=data[data.tagID==i]
    print(data_1_1.head())
    for u in machines:
        macd=[]
        macd.append(u)
        data2=data_1_1[data_1_1.macID==u]
        for f in days:
            data3=data2[data2.rDate==f]
            time_arr=data3["rTime"]
            j=[]
            for i in time_arr:
                l=parse(i)
                j.append(l)
            j.reverse()
            diff=[]
            for i in range(0,len(j)-1):
                h=j[i]-j[i+1]
                diff.append(h)
            diff.append(f)
            diff.reverse()
            for i in range(1,len(diff)):
                diff[i]=str(diff[i])
            for i in range(1,len(diff)):
                k=diff[i].split(':')
                try:
                    if((int(k[0])==0)and(int(k[1])==0)):
                        diff[i]=int(k[2])
                    elif((int(k[0])!=0)or(int(k[1])!=0)):
                        diff[i]=int(k[2])+(int(k[0])*60)+(int(k[1])*60)
                    else:
                        diff[i]="error"
                except ValueError:
                    diff[i]="val error"
            macd.append(diff)
        tagd.append(macd)
    sessions.append(tagd)
print(sessions)

def identify_sessions(session,data):
    info=[]
    for i in session:
        cycl=[]
        emp_id=i[0]
        cycl.append(emp_id)
        for c in range(1,len(i)):
            mac_rec=[]
            mac=i[c][0]
            mac_rec.append(mac)
            for n in range(1,len(i[c])):
                day_rec=[]
                day=i[c][n]
                if(len(day)==1):
                    mac_rec.append(day)
                else:
                    day_rec.append(day[0])
                    data2=data[data.tagID==emp_id]
                    data3=data2[data2.rDate==day[0]]
                    time_arr=data3["rTime"]
                    day.pop(0)
                    w=[]
                    for f in time_arr:
                        l=parse(f)
                        l=str(l)
                        l=l.split()
                        l=l[1]
                        w.append(l)
                    st=False
                    ed=False
                    run=[0,0,0]
                    for q in range(0,len(day)):
                        gap=day[q]
                        if((gap<3)and(gap>-1)):
                            if(st==False):
                                run[0]=w[q]
                                run[1]=gap
                                run[2]=w[q+1]
                                st=True
                            elif(st==True):
                                run[1]=run[1]+gap
                                run[2]=w[q+1]
                        else:
                            if(st==True):
                                day_rec.append(run)
                                st=False
                                run=[0,0,0]
                    if((run[0]!=0)and(run not in day_rec)):
                        day_rec.append(run)
                    mac_rec.append(day_rec)
            cycl.append(mac_rec)
        info.append(cycl)
    return info                

def plot_data(s_data):
    emp_id=s_data.pop(0)
    for i in s_data:
        if(len(i)>1):
            day=i.pop(0)
            ts=[]
            du=[]
            for k in i:
                ts.append(k[0])
                du.append(k[1])
            y_pos = np.arange(len(ts))
            plt.bar(y_pos,du)
            plt.xticks(y_pos,ts)
            for m, v in enumerate(du):
                plt.text(m-0.05 ,v+0.1 ,str(v), color='blue', fontweight='bold')
            title="TagID: "+emp_id+"    Date: "+day
            plt.title(title)
            plt.xlabel('Start Time')
            plt.ylabel('Duration')
            plt.show()
        
def write_to_csv(stat_data):
    csv_data=[]
    csv_data.append(['tagId','macId','date','start_time','duration','end_time'])
    for i in stat_data:
        tag=i.pop(0)
        for e in i:
            mac=e.pop(0)
            for j in e:
                if(len(j)>1):
                    day=j.pop(0)
                    for k in j:
                        csv_data.append([tag,mac,day,k[0],k[1],k[2]])
    print(csv_data)
    csv.register_dialect('myDialect',quoting=csv.QUOTE_ALL,skipinitialspace=True,lineterminator = '\r')
    csv.register_dialect('myDialect2',lineterminator = '\r')

    with open('csv_data.csv', 'w') as f:
        #writer = csv.writer(f, dialect='myDialect')
        #for row in csv_data:
            #writer.writerow(row)
        writer = csv.writer(f,dialect='myDialect2')
        writer.writerows(csv_data)

    f.close()


    
#print(data.head())
def process():
    data.set_index("recID",inplace=True)
    inf=identify_sessions(sessions,data)
    print(inf)
    write_to_csv(inf)
    exit()
    #plot_data(inf[1])
    #print(data.head())

process()

