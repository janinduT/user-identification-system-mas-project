/*
 Basic ESP8266 MQTT example

 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library.

 It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off

 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.

 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

*/

/*
 * ------------------------------------------------------------------------------------------------------
 * 
 * Typical pin layout used:
 * ------------------------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino     NodeMcu v3
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro   Esp8622
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin         Pin
 * ------------------------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST         D3(GPIO 0)(0)
 * SPI SS      SDA(SS)      10            53        D10        10               10          D4(GPOI 2)(2)
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16          D7(GPIO 13)
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14          D6(GPIO 12)
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15          D5(GPIO 14)
 */

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <SPI.h>
#include <MFRC522.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <TimeLib.h>

#define LED 2
#define RST_PIN 0
#define SS_PIN 2
#define MAC_ID "mac0001"

// Update these with values suitable for your network.

const char* ssid = "jt";
const char* password = "jani1996";
const char* mqtt_server = "192.168.43.87";

WiFiClient espClient;
PubSubClient client(espClient);
WiFiUDP ntpUDP;
MFRC522 mfrc522(SS_PIN,RST_PIN);
NTPClient timeClient(ntpUDP,"europe.pool.ntp.org", 19800, 60000);
long lastMsg = 0;
char msg[50];
int count = 0;
unsigned long last_up=0;

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    delay(1000);
    yield();
    digitalWrite(LED,HIGH);
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("'test/topic'", "1hello world");
      // ... and resubscribe
      client.subscribe("'test/topic'");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  SPI.begin();
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  timeClient.begin();
  if(timeClient.update()){
      last_up=millis();
      Serial.println("Time Updated"); 
  }
  mfrc522.PCD_Init();
  mfrc522.PCD_DumpVersionToSerial();
  Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));
}

void loop() {
  if((millis()-last_up)>20000){
    if(timeClient.update()){
      last_up=millis();
      Serial.println("Time Updated"); 
    }else{
      last_up+=10000;
      Serial.println("Time NOT Updated");
    }
  }

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  /*long now = millis();
  if (now - lastMsg > 5000) {
    lastMsg = now;
    ++value;
    snprintf (msg, 50, "1hello world #%ld", value);
    Serial.print("Publish message: ");
    Serial.println(msg);*/
    if(!mfrc522.PICC_IsNewCardPresent()){
      return;
  }

  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }
  count=count+1;
  if(count==20){
    read();
    delay(500);
    yield();
    count=0;
  }
}

void read(){
  //mfrc522.PICC_DumpDetailsToSerial(&(mfrc522.uid));
  //mfrc522.PICC_HaltA();
  byte myU[4];
  for(int i=0;i<4;i++){
    myU[i]=mfrc522.uid.uidByte[i];
  }
  for (byte i = 0; i < 4; i++) {
    if(myU[i] < 0x10)
      Serial.print(F(" 0"));
    else
      Serial.print(F(" "));
    Serial.print(myU[i], HEX);
  }
  Serial.println();
  char str[5] = "";
  array_to_string(myU, 4, str);
  //String sendit = "Uid is ";
  //sendit.concat(str);
  //Serial.println(sendit);
  String timeS=timeClient.getFormattedTime();
  timeS.concat(" ");
  timeS.concat(str);
  timeS.concat(" ");
  String dd = String(day(timeClient.getEpochTime()));
  String mm = String(month(timeClient.getEpochTime()));
  String yy = String(year(timeClient.getEpochTime()));
  String daTe = dd+="/";
  daTe+=mm;
  daTe+="/";
  daTe+=yy;
  timeS.concat(daTe);
  timeS.concat(" ");
  timeS.concat(MAC_ID);
  char fin[50]="";
  timeS.toCharArray(fin,50);
  snprintf (msg, 50,"%s\n",fin);
  client.publish("'test/topic'", msg);
}

void array_to_string(byte array[], unsigned int len, char buffer[]){
    for (unsigned int i = 0; i < len; i++){
        byte nib1 = (array[i] >> 4) & 0x0F;
        byte nib2 = (array[i] >> 0) & 0x0F;
        buffer[i*2+0] = nib1  < 0xA ? '0' + nib1  : 'A' + nib1  - 0xA;
        buffer[i*2+1] = nib2  < 0xA ? '0' + nib2  : 'A' + nib2  - 0xA;
    }
    buffer[len*2] = '\0';
}